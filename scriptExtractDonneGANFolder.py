import csv
import os
import time
import tkinter as tk
from tkinter import filedialog

#On nettoie la console
#Nettoyage des affichages de test
'''
clear = lambda: os.system('cls')
clear()'''

#On change le répertoire de travail
#Cela donne la possibilité d'exécuter le script depuis n'importe où
path_exec = os.path.abspath(__file__)
path_folder = os.path.dirname(path_exec)
os.chdir(path_folder)
os.chdir("..")

#Variable qui permet de vérifier si le programme n'a pas planté
check = True

#Liste de string correspondant au noms des colonnes dans les fichiers csv généré par le GAN
#Utilisé lors de l'écriture du fichier BML
expressionAcronyme = ["AU01_r","AU02_r","AU04_r","AU05_r","AU06_r","AU07_r","AU09_r","AU10_r","AU12_r","AU14_r","AU15_r","AU17_r","AU20_r","AU23_r","AU25_r","AU26_r","AU28_r","AU45_r"]

#Liste de string correpondant au AU, string au format pour le fichier BML
#Utilisé lors de l'écriture du fichier BML
au_bml = ["au0","au1","au3","au4","au5","au6","au8","au9","au11","au13","au14","au16","au19","au22","au24","au25","au27","au44"]

#Liste de string correspondant au numéro des AU
#Utilisé lors de l'écriture du fichier BML
au_bmln = ["1","2","4","5","6","7","9","10","12","14","15","17","20","23","25","26","28","45"]


'''
--------- Déclaration des fonctions
'''

def printlst(lst):
    for line in lst:
        print(line)

def printlstlst(lst):
    for line in lst:
        for case in line:
            print(case)

def extractIntensity(chemin_fichier_csv, nom_colonne_r):
    data = []
    cptTime = 1
    #Ouverture du fichier csv
    with open(chemin_fichier_csv, 'r') as fichier_csv:
        #On recupère les valeurs des colonnes du fichier csv
        lecteur_csv = csv.DictReader(fichier_csv)

        #On regarde chaque ligne du fichier csv
        for ligne in lecteur_csv:
                
                #On ajoute pour chaque ligne le timestamp l'intensité de l'AU, le numéro de l'AU, la valeur d'activation de l'AU et le numéro de l'activation de l'AU
                # ex : donnees_au_r = (" 0.2658"," 0.58"," AU04_r"," 1.00"," AU04_c")
                data.append((round(0.4*cptTime,2),ligne[nom_colonne_r],nom_colonne_r))
                cptTime = cptTime + 1
    return data

def sortLaunchTime(colonne):
    #On trie par ordre croissant les AU pour les mettres dans leur ordre d'activation
    colonne = sorted(colonne,key=lambda x: x[0])
    return colonne


#On prépare la création d'une fenêtre pour nous permettre de sélectionner le dossier que l'on souhaite utilisé
root = tk.Tk()
root.withdraw()

#On affichage la fenêtre d'explorateur de fichier pour demander le dossier que l'utilisateur souhaite transcrire en fichier bml
#On récupère le path du dossier sélectionné
directory_selected = filedialog.askdirectory()

print(directory_selected)


#Si l'utilisateur n'a pas choisi de dossier ou à fermer la fenêtre
#Alors on indique qu'aucun dossier n'a été sélectionné
if not directory_selected:
    print("Not found directory")

#Sinon on exécute le reste du programme
else:
    print("Path directory :", directory_selected)

#On vérifie si le chemin du dossier existe
if(os.path.exists(directory_selected)):
    
    #On récupère la liste des fichiers contenus dans le dossier
    files = os.listdir(directory_selected)
    #On trie les fichiers pour ne garder que les fichiers csv
    files = [f for f in files if(f.endswith('.csv') and os.path.isfile(os.path.join(directory_selected,f)))]
    #Si la liste des fichiers est vide après le trie alors aucun fichier csv n'a été trouvé
    if(len(files) == 0):
        #On passe check à faux parce que le programme n'a crée aucun fichier BML
        check = False
    #On parcours la liste des fichiers csv contenu dans le dossier
    for file in files:
        file_path = directory_selected + "/" + file
        #A chaque passage de boucle on ouvre le fichier csv correspondant
        with open(file_path, 'r') as file:

            #On récupère les données qui y sont stockées
            reader = csv.DictReader(file)

            #On récupère le noms des colonnes et on les stocks dans une liste
            column_names_intensity = [nom_colonne for nom_colonne in reader.fieldnames if nom_colonne.endswith("_r")]


        # TimeStamp, Intensity, NameAU
        data_intensity = [] #Liste qui va stocker l'ensemble des données d'intensité extraite des fichiers csv

        #Pour chaque nom de colonne (ce qui correspond à chaque colonne) on récupère les données corresondante
        for i in range (len(column_names_intensity)):
            #On appelle la méthode extractIntensity pour extraire les données de la colonne concerné
            #Les données sont récupérées sous formes de liste qu'on ajoute ensuite à notre liste contenant toutes nos données
            data_intensity.append((extractIntensity(file_path, column_names_intensity[i])))

        #On récupère le nom du fichier csv utilisé
        file_name = os.path.basename(file_path)
    
        #On regarde si le dossier Fichier_BML existe
        #Si il n'existe pas alors on le crée
        if(not os.path.exists("./Fichier_BML/")):
            os.system("mkdir .\Fichier_BML")

        #On prépare le path du dossier contenant les fichiers BML généré
        path_bml_file = "./Fichier_BML/" + os.path.basename(directory_selected)

        #On regarde si le dossier existe déjà
        #Si il n'existe pas alors on le crée
        #Ce dossier va contenir les fichiers BML générés à partir des données des fichiers csv
        if(not os.path.exists(path_bml_file)):
            os.makedirs(path_bml_file, exist_ok=1)

        #On prépare le path du fichier BML qui va être crée
        csv_file_path = "./Fichier_BML/"+ os.path.basename(directory_selected) + "/" + os.path.splitext(file_name)[0] + ".bml"

        #On crée (ou ouvre si il existe déjà) le fichier BML
        with open(csv_file_path, "w") as file:
            #On écrit dedans le code BML
            #On formate le fichier BML avec le bon format pour être lisible par MARC
            file.write("<bml>\n")
            file.write("\n")
            #On spécifie que c'est l'agent n°1 qui va être utilisé
            file.write("<!-- TRACK : Agent_1 : Main Track-->\n")
            file.write("<!-- ******************************************************************************  -->\n")
            file.write("\n")
            file.write('<marc:fork id="Track_0">\n')
            file.write('\t<marc:agent name="Agent_1" />\n')
            file.write('\n')

            #On initialise le compteur
            cpt = 1

            #Variable qui va stockée le TimeStamp maximum pour déterminer le temps maximum
            TimeMax = 0;

            #On crée une liste qui va régroupé l'ensemble des AU
            #Le but est de faciliter l'écriture du fichier BML
            data = []
            #On parcours chaque liste correspondante à chaque AU
            for lstau in data_intensity:
                #On parcours chaque intensité de l'AU concerné
                for au in lstau:
                    #On stocke la line dans une seule liste
                    data.append(au)

            #On trie les AU en fonction de leur timestamp
            data = sortLaunchTime(data)

            #Pour chaque AU de la liste data
            for au in data:
                #On formate le fichier à l'écriture d'une AU
                file.write('\t<marc:fork>\n')

                #Le moment d'attente avant que l'AU soit activé
                file.write('\t\t<wait duration="'+ str(au[0]) + '" />\n')
                
                #On cherche l'index de l'AU pour pourvoir l'utiliser pour récupérer le bon formatage pour l'écriture de la commande bml
                index = expressionAcronyme.index(au[2])
                
                #On écrit l'AU avec le numéro de l'AU, son intensité, sa durée
                file.write('\t\t<face id="bml_item_'+ str(cpt) +'_'+ au_bml[index] +'"  type="FACS" side="BOTH" amount="'+ str(float(au[1])/1) +'" au="'+ au_bmln[index] +'" marc:interpolate="'+ "0.4" +'" marc:interpolation_type="linear" />\n')
                
                #Balise de fin pour qui signifie que l'AU est terminé
                file.write('\t</marc:fork>\n')

                #Si le temps maximum est inférieur au timestamp en cours alors le timestamp en cours devient le temps maximum
                if(TimeMax < au[0]):
                    TimeMax = au[0]

                #On écrit un clear pour retirer l'AU du visage de l'agent virtuel
                '''file.write('\t<marc:fork>\n')
                file.write('\t\t<wait duration="'+ str(format(tempValue,".3f"))+ '" />\n')
                file.write('\t\t<face id="bml_item_'+ str(cpt) +'" type="FACS" side="BOTH" amount="0" au="'+ au_bmln[index] +'" marc:interpolate="1.0" />\n')
                file.write('\t</marc:fork>\n')'''
                cpt = cpt + 1
        
            #On écrit le formatage pour finir le fichier bml avec la bonne syntaxe
            file.write('\t<marc:fork>\n')

            #On attend la fin de l'expression des AU pour remettre le visage de l'agent virtuel à une expression neutre
            file.write('\t\t<wait duration="'+ str(TimeMax) + '" />\n')
            file.write('\t\t<face id="bml_item_'+ str(cpt+1) +'" type="FACS" side="BOTH" amount="0" au="marc:ALL" marc:interpolate="1.0" />\n')
            file.write("\t</marc:fork>\n")
            file.write("</marc:fork>\n")
            file.write("</bml>")
        
        print("BML File success writing :", os.path.basename(csv_file_path))
    
    #On vérifie si les programmes c'est éxécuté correctement, si c'est le case alors on affiche le dossier ou se situe les fichiers générés
    if(check):
        print("You can find your file in " + path_bml_file)
    else:
        print("No csv file found in the directory")