
#Importation des librairies
import csv
import os
import time
import tkinter as tk
from tkinter import filedialog


#///////// Variables


donnees_au_c = []

#Liste qui contient les AU et leurs intensités
donnees_au_r = []

#Liste principale qui va contenir les AU qui apparaissent dans le vidéo avec leur timestamp, leur intensité, leur durée et le nom de l'AU
duree=[]

#Liste de string correspondant au nom des colonnes dans le fichier .csv généré par OpenFace
#Utilisé lors de l'écriture du fichier BML
expressionAcronyme = [" AU01_c"," AU02_c"," AU04_c"," AU05_c"," AU06_c"," AU07_c"," AU09_c"," AU10_c"," AU12_c"," AU14_c"," AU15_c"," AU17_c"," AU20_c"," AU23_c"," AU25_c"," AU26_c"," AU28_c"," AU45_c"]

#Liste de string correpondant au AU, string au format pour le fichier BML
#Utilisé lors de l'écriture du fichier BML
au_bml = ["au0","au1","au3","au4","au5","au6","au8","au9","au11","au13","au14","au16","au19","au22","au24","au25","au27","au44"]

#Liste de string correspondant au numéro des AU
#Utilisé lors de l'écriture du fichier BML
au_bmln = ["1","2","4","5","6","7","9","10","12","14","15","17","20","23","25","26","28","45"]


#///////// Fonction

#Fonction qui permet d'afficher une liste
def printlst(lst):
    for ligne in lst:
        print(ligne)

'''
   Fonction qui repère les itensites de l'AU passé en paramètres
   Les données sont récupérées depuis le fichier csv qui est généré après l'analyse d'OpenFace
'''

def extractIntensity(chemin_fichier_csv, nom_colonne_r, nom_colonne_c):
    
    #Ouverture du fichier csv
    with open(chemin_fichier_csv, 'r') as fichier_csv:
        #On recupère les valeurs des colonnes du fichier csv
        lecteur_csv = csv.DictReader(fichier_csv)

        #On regarde chaque ligne du fichier csv
        for ligne in lecteur_csv:
                
                #On ajoute pour chaque ligne le timestamp l'intensité de l'AU, le numéro de l'AU, la valeur d'activation de l'AU et le numéro de l'activation de l'AU
                # ex : donnees_au_r = (" 0.2658"," 0.58"," AU04_r"," 1.00"," AU04_c")
                donnees_au_r.append((ligne[" timestamp"],ligne[nom_colonne_r],nom_colonne_r,ligne[nom_colonne_c],nom_colonne_c))
                
    return donnees_au_r

'''
    Fonction qui calcule l'intensité maximum leur de l'apparition de chaque AU
    L'intensité maximal sera utiliser pour l'écriture du fichier BML
'''
def calculMaxIntensite():
    
    #Liste qui va stocker l'intensité maximale de l'AU et le nom de l'AU concerné
    data = []

    #Variable qui stocke l'intensité maximale d'une expression
    max = 0

    #Varibale qui va valeur précédente correspondante à l'activation ou non de l'AU
    tempVal = 0 # 0 ou 1

    tempsAU = " "
    #Ouverture du fichier data pour stocker les valeurs
    #Test pour vérifier les valeurs
    with open("data.txt", "w") as fichier:
        #On parcours chaque ligne récupéré dans le fichier csv
        for ligne in donnees_au_r:
                
                #On regarde si l'AU est présente
                if(float(ligne[3])==1):
                    #On écrit la ligne dans le fichier
                    fichier.write(str(ligne) + "\n")

                    #On regarde si l'intensité de la ligne est supérieur à l'intensité maximum
                    #Si elle l'est alors on remplace le max par cette valeur parce que ce n'est plus l'intensité la plus grande
                    if(max<float(ligne[1])):
                        max = float(ligne[1])

                #Si l'AU etait présente à la frame d'avant alors et qu'elle n'est plus présente à la frame actuelle alors
                #cela veut dire que l'AU est terminé
                elif(tempVal == 1 and float(ligne[3]) == 0):
                    #On écrit l'intensité maximale et l'AU concerné dans le fichier
                    fichier.write("Max Intensite : " + str(max) + " AU = " + tempsAU + "\n")
                    #On saute une ligne pour faire apparaître des "groupes "
                    fichier.write("\n")
                    #On ajoute à la liste l'AU avec son intensité maximale
                    data.append((max,tempsAU))
                    #On remet le max à 0 pour la prochaine AU
                    max = 0
                #On stocke la valeur d'activation de l'AU pour pouvoir l'utilisé lors de la comparaison
                tempsAU = ligne[4]
                tempVal = float(ligne[3])
    max = 0

    #On retourne la liste des AU avec leur intensité maximale
    return data

'''
    Fonction qui regarde pour chaque expression passée en parametre les donnees
       -   le temps de debut de l'expression
       -   la duree de l'expression
'''
def factoData(colonne_extraite, nom_colonne):
    #Liste qui va stocke les AU avec le timestamp, sa durée et son numéro
    colonne = []

    #Variable temporaire qui va stocker la soustraction du timestamp de fin et du timestamp de départ pour déterminer la durée de l'AU
    time = 1

    #Varibale qui va valeur précédente correspondante à l'activation ou non de l'AU
    value = 0

    #Variable qui va stocker le temps à laquelle l'AU démarre
    minTime = 0


    #cpt = 0

    #On parcours chaque ligne de la colonne extraite
    for ligne in colonne_extraite:
        #Si l'AU est activé et si le minTim est à 0
        #Si c'est la cas alors cela veut dire qu'une AU vient de commencer
        #On stocke donc le timestamp de départ
        if(float(ligne[1]) != 0 and minTime == 0):
            #cpt = cpt + 1
            minTime = float(ligne[0])

        #Sinon on regarde si l'AU est désactivé
        elif(float(ligne[1]) == 0):
            
            #Si c'est le cas alors on regarde si la valeur précédente était 1
            #Si c'est le cas alors cela veut dire que l'AU vient de terminer
            #Nous devons donc stocke le timestamp de départ et la durée de l'AU
            if(float(value) != 0):

                #On récupère la valeur du timestamp actuel
                maxTime = float(ligne[0])

                #On calcul la durée de l'AU en faisant le timestamp de fin - le timestamp de départ
                time = maxTime - minTime

                #On stocke alors le timestamp de départ, la durée, et le nom de l'AU
                #ex : colonne = (1.5,6.326," AU07_c")
                colonne.append((time,minTime,nom_colonne))
                
                #On remet le timestamp de départ à 0
                minTime = 0
        
        #Si la ligne de données que l'on regarde est la dernière et que le dernière valeur d'activation était un 1 alors on enregistre l'AU
        elif(float(ligne[1]) == 1 and ligne == colonne_extraite[len(colonne_extraite)-1]):
            colonne.append((time,minTime,nom_colonne))

        #On garde en mémoire la valeur d'activation de la ligne d'avant
        value = ligne[1]
    return colonne

'''
    Fonction qui regarde pour chaque expression passée en parametre les données
       -   le temps de debut de l'expression
       -   la duree de l'expression
    Renvoie le temps et la valeur associé à la présence de l'expression (0 ou 1)
'''
def extractData(chemin_fichier_csv, nom_colonne):

    #Liste qui va stocker les données (timestamp, activation, nom AU) que l'on va récupérer du fichier csv
    colonne = []

    #On lit le fichier csv
    with open(chemin_fichier_csv, 'r') as fichier_csv:
        
        #On récupère les lignes du fichier csv
        lecteur_csv = csv.DictReader(fichier_csv)

        #On parcours chaque ligne du fichier csv
        for ligne in lecteur_csv:
            #On stocke temporairement la valeur d'activation de l'AU qui a été passé en paramètre 
            value = ligne[nom_colonne]
            #On stocke temporairement le timestamp
            timestamp = ligne[" timestamp"]

            #On stocke le timestamp et la value d'activation de l'AU
            colonne.append((timestamp,value))
    return colonne

'''
    Fonction qui trie les expressions en fonction du temps auxquelles elles apparaissent
    Trie dans l'ordre croissant
    Renvoie une liste avec les expressions dans l'ordre croissante de leur apparition
'''
def sortLaunchTime(colonne_extraite):
    #On trie par ordre croissant les AU pour les mettres dans leur ordre d'activation
    colonne = sorted(colonne_extraite,key=lambda x: x[1])
    return colonne

'''
    Fonction qui prend en paramètre une liste d'expressions triée par ordre croissante de leurs apparition
    Calcul les temps d'attente entre chaque expressions pour les wait du fichier BML
    Renvoie une liste contenant le temps d'attente entre chaque expressions
'''
def calculWaitTime(duree):
    
    #Liste qui va stocker la durée entre chaque AU
    time = []

    #Paramètre qui va nous permettre d'arrêtre notre boucle
    i = 1

    #On stocke la première valeur
    time.append(duree[0][1])

    #Tant qu'il reste des lignes dans la liste durée, on la parcours
    while i < len(duree):
        #On récupère le timestamp de l'AU de la ligne
        temp = duree[i][1]
        #On ajout la valeur
        time.append(temp)
        i = i + 1
    return time         


'''
    -----> START OF THE PROGRAM
'''

#On replace notre répertoire de travail dans le bon dossier pour éxécuter le script python et trouve les bon fichiers au bon endroit et éviter les erreurs de PATH
path_exec = os.path.abspath(__file__)
path_folder = os.path.dirname(path_exec)
os.chdir(path_folder)
os.chdir("..")

#PATH du dossier contenant les videos
dossier_path = "./OpenFace_2.2.0_win_x64/video"

#Declaration de l'attribut check qui va servir a verifier si un fichier video a ete trouve
check = False


'''
    On demande à l'utilisateur de sélectionner un fichier vidéo
   -   .AVI
   -   .MP4
'''

#On prépare à ouvrir une fenêtre
root = tk.Tk()
root.withdraw()

#Préparation des paramètres pour n'affichier que les fichiers qui nous peuvent être utilisé par OpenFace
filetypes = (
    ('video files', '*.mp4'),
    ('avi files', '*.avi'),
    (' mov files', '*.mov')
)

#Affichage de l'explorateur de fichier avec les paramètres de types de fichiers
file_path = filedialog.askopenfilename(
    title='Choose the video, you want to analyse',
    filetypes=(("Fichiers vidéo", "*.mp4 *.avi *.mov"),("Fichiers mp4", "*mp4"),("Fichiers avi", "*avi"),("Fichiers mov", "*.mov"))
)

# Affichage du path du fichier sélectionner par l'utilisateur
print(file_path)

'''
    Si on ne trouve pas de fichier video dans le dossier, on ne fait rien
    Sinon on l'analyse grâce à OpenFace
'''
#On regarde si on fichier a été sélectionné par l'utilisateur
#Si il a juste fermé la fenêtre sans sélectionner de fichier alors on affiche que l'on a trouvé aucun fichier
if not file_path:

    print("Not found MP4 or AVI file")
else:
    check = False

    '''
        On demande à l'utilisateur la video qu'il souhaite analyser
    '''
    #On affiche le fichier sélectionné par l'utilisateur
    print("You have selected :",os.path.basename(file_path))
    print("Starting... ")
    time.sleep(2)
    
    #On récupère le path du fichier dans une variable
    file_path = file_path

    #On stocke le nom du fichier avec l'extension
    file_name = os.path.basename(file_path)

    #On stocke le nom du fichier sans l'extension pour pouvoir l'utiliser plustard
    file_name_n = os.path.splitext(file_name)[0]

    #Affichage du PATH et du fichier sélectionné
    print("File_PATH : ", file_path)
    print("File_NAME : ", file_name)

    #On a bien trouvé un fichier on passe check à vrai
    check = True

#Si un fichier vidéo à bien été trouvé
if(check):

    #On se place dans le bon répertoire pour éxécuter OpenFace et générer les fichiers au bon endroit
    path = os.path.join(os.getcwd(), "OpenFace_2.2.0_win_x64")
    os.chdir(path)
    
    #On lance l'analyse avec OpenFace
    os.system('.\FeatureExtraction.exe -f "' + file_path + ' ')

    #Une fois la video analysée par OpenFace

    #On crée les variables pour stocker les path des fichiers .csv (lecture) .bml(création)
    dossier = "processed"
    chemin_fichier_csv = " "
    chemin_fichier_bml = " "

    #On récupère la liste des fichiers csv présent dans le dossier
    fichier_csv = [f for f in os .listdir(dossier) if f.endswith('.csv')]

    '''
        Si il n'y a aucun fichier csv dans le dossier alors on indique qu'aucun fichier csv n'a été trouvé
        
        Sinon on recupère le fichier csv qui porte le nom de la vidéo qui a été analysée avec OpenFace
        et on prépare le path et le nom du fichier bml qui sera crée
    '''
    if not fichier_csv:
        print("Not found CSV file in : ", dossier)
    else:
        #On prépare le nom du fichier csv en utilisant le même nom que la vidéo sélectionnée
        file_csv = file_name_n + ".csv"

        #On prépare le path du fichier csv.
        path_file_csv = os.path.join(dossier,file_csv)

        #On regarde si le fichier csv généré par OpenFace existe
        chemin_fichier_csv = os.path.exists(path_file_csv)

        #On stocke le path du fichier csv dans une autre variable
        chemin_fichier_csv = path_file_csv

        #On affiche le path et le nom du fichier csv, que l'on va utilisé
        print("PATH : ", chemin_fichier_csv)
        print("File CSV Name : ", file_name_n)

        #Préparation des dossiers et du fichier BML qui va être généré
        #Si le dossier Fichier_BML n'existe pas alors on le crée
        if(not os.path.exists("../Fichier_BML/")):
            os.system("mkdir ..\Fichier_BML")

        #Si le dossier OpenFace_BML_File n'exsite pas alors on le crée
        if(not os.path.exists("../Fichier_BML/OpenFace_BML_File")):
            os.system('mkdir ..\Fichier_BML\OpenFace_BML_File')
        
        #On prépare le chemin du futur fichier bml
        chemin_fichier_bml = '..\Fichier_BML\OpenFace_BML_File/' + file_name_n + ".bml"

    '''
        On lit le fichier csv pour y récupérer les données des intensités des expressions
    '''
    #On ouvre le fichier BML pour lire les données
    with open(chemin_fichier_csv, 'r') as fichier_csv:
        #On récupère les lignes de données du fichier
        lecteur_csv = csv.DictReader(fichier_csv)
        print("Data Extraction...")
        
        #On récupère le nom des colonnes correpondant au intensité des AU (ex : AU01_r)
        noms_colonnes_r = [nom_colonne for nom_colonne in lecteur_csv.fieldnames if nom_colonne.endswith("_r")]

        #On récupère le nom de colonnes correspondant au activtion des AU (ex : AU01_c)
        noms_colonnes_c = [nom_colonne for nom_colonne in lecteur_csv.fieldnames if nom_colonne.endswith("_c")]

        #On modifie le noms de l'avant dernière colonne des AU intensités parce que OpenFace ne pas l'intensité pour l' AU28_c
        noms_colonnes_c[len(noms_colonnes_r)-1] = " AU45_c"
        #printlst(noms_colonnes_r)

        #On parcourt la liste de nom que l'on vient de récupérer
        for i in range(len(noms_colonnes_r)):
        
            #Pour chaque AU on extrait du fichier ses intensités que l'on stocke dans une liste
            donnees_au_r = (extractIntensity(chemin_fichier_csv, noms_colonnes_r[i], noms_colonnes_c[i]))
        #printlst(donnees_au_r)

        #On calcul l'intensité maximum pour chaque AU présente
        dataIntensite = calculMaxIntensite()

    '''
        On récupère la présence et le temps d'apparation de chaque expression depuis le fichier csv
    '''
    #On lit le fichier csv dans le but de récupérer les données correspondante au activation des AU
    with open(chemin_fichier_csv, 'r') as fichier_csv:
        lecteur_csv = csv.DictReader(fichier_csv)

        #On récupère les noms des colonnes correpondant au activation des AU (ex : AU01_c)
        noms_colonnes_c = [nom_colonne for nom_colonne in lecteur_csv.fieldnames if nom_colonne.endswith("_c")]

        #Création d'un liste temporaire qui va stocker les données
        temp = [] #Va stocker l'ensemble des AU dans l'ordre des croissantes de leur numéro

        #Pour chaque AU de la liste récupéré
        for nom_colonne in noms_colonnes_c:

            #On extrait les données correspondante au activation des AU
            colonne_extraite = extractData(chemin_fichier_csv,nom_colonne)

            #On factorise les données récupérés pour ne garder que les moments ou les AU son activé et l'on calcule la durée de leur activation et le moment ou elle se déclenche
            #On regroupe les frames ou les AU sont activées pour calculer leur temps d'activation
            colonne_extraite = factoData(colonne_extraite,nom_colonne)

            #Si la liste contenant les activations de l'AU est vide cela signifie qu'elle n'est pas présentes
            #Si la taille de la liste est supérieur à 0 alors l'AU est présente, on l'ajout donc à notre liste temporaire
            if(len(colonne_extraite)> 0):
                
                #Pour chaque activation de l'AU
                for ligne in colonne_extraite:
                    #On ajoute l'AU à la liste des AU
                    temp.append(ligne)
        
        #Création d'un liste qui va stocker les AU avec leur durée et leur intensité correpondante et le timestamp de leur activation
        temps = []

        #Compteur qui entervient si l'AU28 est présente car elle n'a pas d'intensité on fixe donc son intensité à 1 par défault
        #Cela nous permet de ne pas avancé dans notre liste d'intensité des AU
        j = 0
        for i in range(len(temp)):
            #print("temp[",i,"][2] = ",temp[i][2] , "dataIntensite[",j,"][1] = ", dataIntensite[j][1])
            
            #Si l'AU d'activation correspond à la même que l'AU d'intensité alors c'est qu'elle correpond
            if(temp[i][2] == dataIntensite[j][1]):
                #On regroupe les données de l'AU dans la même ligne de la liste globale
                temps.append((temp[i][0],temp[i][1],temp[i][2],dataIntensite[j][0]))
                j = j + 1
            #Si les noms ne correpondent pas alors c'est que cela correpond à l'AU28 on fixe donc son intensité à 1
            else:
                temps.append((temp[i][0],temp[i][1],temp[i][2],1.0))
            i = i + 1
        
        #On trie les AU par leur ordre d'activation
        duree = sortLaunchTime(temps)

        #On calcule le temps d'attente entre les AU
        time = calculWaitTime(duree)

        ''' Fichier txt de TEST '''
        with open('AUTempCroissant.txt', 'w') as fichier_txt_AU:
            for ligne in duree:
                fichier_txt_AU.write(str(ligne) + '\n')


        #print("Durée")
        #printlst(duree)
        
        for frame in lecteur_csv:
            for nom_colonne in noms_colonnes_c:
                if(frame[nom_colonne] == " 1.0"):
                    donnees_au_c.append((frame["frame"],nom_colonne))
    print("Data extract")

    '''
        On écrit le fichier BML en fonction des données extraites du fichier csv
    '''
    print("Writing BML File...")
    with open(chemin_fichier_bml, "w") as fichier_bml:

        '''
            On formate le fichier BML au bon format lisible par MARC
        '''
        #On écrit l'entête du fichier
        fichier_bml.write("<bml>\n")
        fichier_bml.write("\n")

        #On spécifie que c'est l'agent n°1 qui va être utilisé
        fichier_bml.write("<!-- TRACK : Agent_1 : Main Track-->\n")
        fichier_bml.write("<!-- ******************************************************************************  -->\n")
        fichier_bml.write("\n")
        fichier_bml.write('<marc:fork id="Track_0">\n')
        fichier_bml.write('\t<marc:agent name="Agent_1" />\n')
        fichier_bml.write('\n')
        i = 0
        cpt = 1
        cptTime = 0

        '''
            On écrit les expressions en fonction de leur moment d'apparation, de leur durée et de leur intensité
            On ajoute ensuite un clear pour supprimer l'expression une fois que celle-ci est terminé
        '''
        
        #Pour chaque AU de la liste
        for frame in duree:
            fichier_bml.write('\t<marc:fork>\n')

            #Le moment d'attente avant que l'AU soit activé
            fichier_bml.write('\t\t<wait duration="'+ str(format(time[cptTime],".3f"))+ '" />\n')
            index = expressionAcronyme.index(frame[2])
            
            #On échelonne l'intensité des expressions
            if(frame[3] >= 0 and frame[3] <= 5):
                print(frame[3])
            if(frame[3] >= 1 and  frame[3] <= 2):
                intensite = frame[3] / 2.5
            elif(frame[3] > 2 and frame[3] <= 5):
                intensite = frame[3] / 2.5
            else:
                intensite = frame[3] / 2.5
            
            #fichier_bml.write('\t\t<face id="bml_item_'+ str(cpt) +'_'+ au_bml[index] +'"  type="FACS" side="BOTH" amount="'+ str(format(intensite,".5f"))+'" au="'+ au_bmln[index] +'" marc:interpolate="'+ str(format(frame[0],".3f")) +'" marc:interpolation_type="linear" />\n')
            
            #On écrit l'AU avec le numéro de l'AU, son intensité, sa durée
            fichier_bml.write('\t\t<face id="bml_item_'+ str(cpt) +'_'+ au_bml[index] +'"  type="FACS" side="BOTH" amount="'+ str(intensite) +'" au="'+ au_bmln[index] +'" marc:interpolate="'+ str(format(frame[0],".3f")) +'" marc:interpolation_type="linear" />\n')
            
            fichier_bml.write('\t</marc:fork>\n')

            #On écrit un clear pour retirer l'AU du visage de l'agent virtuel
            fichier_bml.write('\t<marc:fork>\n')
            tempValue = time[cptTime] + frame[0]
            fichier_bml.write('\t\t<wait duration="'+ str(format(tempValue,".3f"))+ '" />\n')
            fichier_bml.write('\t\t<face id="bml_item_'+ str(cpt) +'" type="FACS" side="BOTH" amount="0" au="'+ au_bmln[index] +'" marc:interpolate="1.0" />\n')
            fichier_bml.write('\t</marc:fork>\n')
            cpt = cpt + 1
            cptTime = cptTime + 1
        
        '''
            On formate la fin du fichier pour qu'il soit lisible par MARC
        '''

        #On écrit la fin du fichier pour qu'il soit au bon format
        fichier_bml.write('\t<marc:fork>\n')
        tempValue = time[cptTime-1] + duree[len(duree)-1][0]
        fichier_bml.write('\t\t<wait duration="'+ str(format(tempValue,".3f"))+ '" />\n')
        fichier_bml.write('\t\t<face id="bml_item_'+ str(cpt+1) +'" type="FACS" side="BOTH" amount="0" au="marc:ALL" marc:interpolate="1.0" />\n')
        fichier_bml.write("\t</marc:fork>\n")
        fichier_bml.write("</marc:fork>\n")
        fichier_bml.write("</bml>")
    print("Write BML success file !")

    #PATH ou le fichier est stocké
    print("PATH :",chemin_fichier_bml)