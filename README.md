# Python Transcribe OpenFace TO MARC

Ce script python a pour objectif d'utiliser OpenFace pour générer des fichiers BML utilisables par le logiciel MARC.
Ce script va donner une vidéo à OpenFace pour extraire les données des expressions et leur intensité dans un fichier CSV, que le script va ensuite analyser.
Il va récupérer les valeurs des intensités et le moment où l'expression apparaît ainsi que la durée de son apparition et va générer un fichier BML interprétable par MARC.

## Installation

Pour installer le script Python sur votre ordinateur.

Vous avez deux méthodes:

- Télécharger le [dépôt](https://gitlab.lisn.upsaclay.fr/stadler/python-transcribe-openface-to-marc.git).

- Cloner le [dépot](https://gitlab.lisn.upsaclay.fr/stadler/python-transcribe-openface-to-marc.git).

![Repository Logo](imgs/Readme/Installation.png)

## Utilisation

- Si vous avez téléchargé le .zip, décompressez le et placer le dossier dans le même répertoire où se situent vos dossiers OpenFace et MARC.

- Si vous avez cloné le dossier déplacer le dans le même répertoire où se situent vos dossiers OpenFace et MARC.

### Prerequis

Pour exécuter le script, vous devez utiliser la version 3.9.19 de python

Vous devez avoir installé OpenFace son dossier actuel, si vous avez change l'emplacement du dossier OpenFace vous devez refaire l'[installation](https://github.com/TadasBaltrusaitis/OpenFace/wiki/Model-download)

Pour utiliser le script vous devez placer les dossiers contenant MARC, OpenFace et le script Python dans le même dossier

![Folder Screen](imgs/Readme/Folder_Emplacement.png)


### Exécution du script

Pour lancer le script ouvrez un terminal placez vous dans le répertoire où se trouve votre script python et exécutez la commande

```
python scriptExtractDonneSelectedFile.py
```

Une fois le script lancé, vous devrez choisir la vidéo que vous souhaitez analyser.

![Script Exec](imgs/Readme/ScriptExec.png)

Une fois le script terminé, vous pourrez trouver un dossier "Fichier_BML/OpenFace_BML_File/" contenant le fichier BML généré à partir de la vidéo.

![Folder BML](imgs/Readme/Folder_BML.png)